var count = 0;
var pageNum = 0;
var rightTableFlexCount = 0;
var leftTableFlexCount = 0;
$(function(){
    $("#startButton").on("click",startLomda) 
})

function startLomda(event){
    pageNum = 1;
    $("#startButton").off("click",startLomda) 
    $("#openSquare").fadeOut();
    $("#targets").fadeIn(2000);
    $("#nextButton").on("click",continueLomda);
}
function continueLomda(event) {
    
    console.log(pageNum)
    $("#firstStep").off( "click", continueLomda );
    if (count > 0 ) {
        $("#firstStep").css("opacity" , "100%")
        $("#WhatIsResearch").fadeIn(1000)
        $("#secondStep").css("opacity" , "50%")
        if (pageNum == 4){
        $("#diffrenceBetweenTitle").fadeOut();
        $("#flipCard1").slideUp();
        $("#flipCard2").slideUp();
        $("#diffrenceBetweenButton").fadeOut();
        $("#secondStep").on("click",differenceBetween);
        }
        if (pageNum == 5){
            $("#institutionsTitle").fadeOut();
            $("#sideTable").css("opacity","0");
            $("#judyInvestigation").slideUp(1000);
            $("#pikudInvestigation").slideUp(1000);
            $("#institutionsButton").fadeOut();
            $("#leftTableFlex").css("opacity","0");
            $("#rightTableFlex").css("opacity","0");
            rightTableFlexCount = 0
            leftTableFlexCount = 0
        }
    }
    $("#nextButton").off("click",continueLomda);
    if (count == 0){
    $(".targetSquare").addClass("animate__animated animate__bounceOutDown");
    $("#targetsTitle").addClass("animate__animated animate__bounceOutUp animate__delay-1s");
    $("#targetPhoto").addClass("animate__animated animate__bounceOutUp animate__delay-1s");
    $("#nextButton").addClass("animate__animated animate__bounceOutDown");
    setTimeout(function(){
         $("#firstStep").show();
         $("#firstStep").addClass("animate__animated animate__slideInRight");
         $("#WhatIsResearchTitle").show();
         $("#WhatIsResearchText1").show();
         $("#WhatIsResearchText2").show();
         $("#WhatIsResearchNextButton").show();
        },3000)
    }
    pageNum = 2;
    $("#WhatIsResearchNextButton").on("click",whatIsResearch)
}

function whatIsResearch(event) {
    
    console.log(pageNum)
    if (count > 0 ) {
        $("#veryityResearch").fadeIn(1000)
    }
    $("#WhatIsResearchNextButton").off("click",whatIsResearch)
    $("#WhatIsResearch").fadeOut(1000)
    setTimeout(function(){
    $("#veryityResearchTitle").show()
    },1000)
    setTimeout(function(){
        $(".list").show()
        $("p").show()
        },2000)
    setTimeout(function(){
        $("#modelTitle").show();
        $("#icebergModel").show();
        $("#nextButtonVeryityResearch").show();
         },3000)
         pageNum = 3;
         $("#nextButtonVeryityResearch").on("click",differenceBetween)
}

function differenceBetween(event) {
    
    var flipCardCounter1 = 0;
    var flipCardCounter2 = 0;
    $("#secondStep").css("opacity" , "100%")
    $("#nextButtonVeryityResearch").off("click",differenceBetween) 
    $("#veryityResearch").fadeOut(1000)
    $("#secondStep").show(1000);
    $("#secondStep").addClass("animate__animated animate__slideInRight animate__delay-1s");
    $("#firstStep").css("opacity" , "50%")
    $("#firstStep").css("border-color" , "green")
    $("#firstStep").css("background-color" , "green")
    $("#firstStep").on("click",continueLomda)
    count ++;
    setTimeout(function(){
        $("#diffrenceBetweenTitle").show();
    },2000)
    setTimeout(function(){
        $("#flipCard1").show();
        $("#flipCard2").show();
        $("#diffrenceBetweenButton").prop('disabled', true);
        $("#diffrenceBetweenButton").show();
    },3000)
    $("#flipCard1").mouseover(function(){flipCardCounter1++
        if ((flipCardCounter1 > 0 ) && (flipCardCounter2 > 0 )) {
            $("#diffrenceBetweenButton").prop('disabled', false);

        }});
    $("#flipCard2").mouseover(function(){flipCardCounter2++
        if ((flipCardCounter1 > 0 ) && (flipCardCounter2 > 0 )) {
            $("#diffrenceBetweenButton").prop('disabled', false);
        }});
        pageNum = 4;
    $("#diffrenceBetweenButton").on("click", idfInvestigation)
}   

function idfInvestigation(event) {
   
    console.log(pageNum)
    $("#diffrenceBetweenButton").off("click", idfInvestigation);
    $("#diffrenceBetweenTitle").fadeOut(1000);
    $("#flipCard1").slideUp(1000);
    $("#flipCard2").slideUp(1000);
    $("#diffrenceBetweenButton").fadeOut(1000);
    $("#institutionsTitle").show(1000);
    $("#sideTable").addClass("animate__animated animate__lightSpeedInRight animate__delay-2s")
    $("#sideTable").css("opacity","100");
    $("#judyInvestigation").show(1000);
    $("#pikudInvestigation").show(1000);
    $("#institutionsButton").show();
    setTimeout(function(){
        $("#judyInvestigation").removeClass("animate__fadeInDownBig animate__delay-2s")
        $("#pikudInvestigation").removeClass("animate__fadeInDownBig animate__delay-2s");
        $("#judyInvestigation").addClass("animate__flash");
        $("#pikudInvestigation").addClass("animate__flash");
    },4000);
    $("#judyInvestigation").on("click",showRightTable)
    $("#pikudInvestigation").on("click",showLeftTable)
    pageNum = 5;
    $("#institutionsButton").on("click",concepts)
   
}

function showRightTable(event) {
    rightTableFlexCount++
    if (rightTableFlexCount == 1) {
        $("#rightTableFlex").addClass("animate__animated animate__bounceInDown ")
        $("#rightTableFlex").css("opacity","100");
    }else {
        $("#rightTableFlex").fadeToggle()
    }
}
function showLeftTable(event) {
    leftTableFlexCount++
    if (leftTableFlexCount == 1) {
        $("#leftTableFlex").addClass("animate__animated animate__bounceInDown ")
        $("#leftTableFlex").css("opacity","100");
    }else {
        $("#leftTableFlex").fadeToggle()
    }
}

function concepts (event) {
    pageNum = 6;
    console.log(pageNum)
    $("#institutionsTitle").fadeOut();
    $("#judyInvestigation").fadeOut();
    $("#pikudInvestigation").fadeOut();
    $("#sideTable").css("opacity","0");
    $("#leftTableFlex").fadeOut()
    $("#rightTableFlex").fadeOut()
    // $("#institutionsButton").fadeOut(1000);
}